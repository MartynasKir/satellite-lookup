package com.martynas.satellite.lookup;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import com.martynas.satellite.lookup.services.SatelliteService;
import io.quarkus.test.junit.QuarkusTest;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class SatelliteResourceTest {

    @Inject
    SatelliteService satelliteService;

    @Test
    public void getSatelliteById_returnResultIfExist() {
        String internationalesignator = "1992-001B_get_satellite";

        String payload = "{\"internationalDesignator\":\"1992-001B_get_satellite\","
                + "\"name\":\"Sat1\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO\"}";

        given().body(payload).contentType("application/json")
                .when().post("/v1/satellites/");

        given()
                .when().get("/v1/satellites/" + internationalesignator)
                .then()
                .statusCode(200)
                .body(is(payload));

        satelliteService.deleteSatellite("1992-001B_get_satellite");
    }

    @Test
    public void getSatelliteById_returnErrorIfNotExist() {
        String internationalesignator = "1992-001B_get_satellite";

        given()
                .when().get("/v1/satellites/" + internationalesignator)
                .then()
                .statusCode(204);
    }


    @Test
    public void addSatelliteTest_returnRecordOnSuccess() {

        String payload = "{\"internationalDesignator\":\"1992-001B_add_satellite\","
                + "\"name\":\"Sat1\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO\"}";

        given().body(payload).contentType("application/json")
                .when().post("/v1/satellites/")
                .then()
                .statusCode(200)
                .body(is(payload));

        satelliteService.deleteSatellite("1992-001B_add_satellite");
    }

    @Test
    public void addSatelliteTest_returnErrorIfAlreadyExists() {

        String payload = "{\"internationalDesignator\":\"1992-001B_add_satellite\","
                + "\"name\":\"Sat1\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO\"}";

        given().body(payload).contentType("application/json")
                .when().post("/v1/satellites/");

        given().body(payload).contentType("application/json")
                .when().post("/v1/satellites/")
                .then()
                .statusCode(409);

        satelliteService.deleteSatellite("1992-001B_add_satellite");
    }

    @Test
    public void updateSatelliteTest_returnUpdatedRecordsCountIfSuccess() {

        String existingRecord = "{\"internationalDesignator\":\"1992-001B_update_satellite\","
                + "\"name\":\"Sat1\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO\"}";

        given().body(existingRecord).contentType("application/json")
                .when().post("/v1/satellites/");

        String updatePayload = "{\"internationalDesignator\":\"1992-001B_update_satellite\","
                + "\"name\":\"Sat1_UPDATED\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO_UPDATED\"}";

        given().body(updatePayload).contentType("application/json")
                .when().put("/v1/satellites/")
                .then()
                .statusCode(200)
                .body(is("Successfully updated records: 1"));

        satelliteService.deleteSatellite("1992-001B_update_satellite");

    }

    @Test
    public void updateSatelliteTest_returnErrorIfNotFound() {

        String updatePayload = "{\"internationalDesignator\":\"1992-001B_update_satellite\","
                + "\"name\":\"Sat1_UPDATED\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO_UPDATED\"}";

        given().body(updatePayload).contentType("application/json")
                .when().put("/v1/satellites/")
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteSatellite_returnOK_OnSuccess() {

        String internationalesignator = "1992-001B_delete_satellite";

        String existingRecord = "{\"internationalDesignator\":\"1992-001B_delete_satellite\","
                + "\"name\":\"Sat1\","
                + "\"yearOfLaunch\":1992,"
                + "\"orbitType\":\"GEO\"}";

        given().body(existingRecord).contentType("application/json")
                .when().post("/v1/satellites/");

        given()
                .when().delete("/v1/satellites/" + internationalesignator)
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteSatellite_returnErrorIfNotFound() {

        String internationalesignator = "1992-001B_delete_satellite_not_exist";

        given()
                .when().delete("/v1/satellites/" + internationalesignator)
                .then()
                .statusCode(404);
    }
}
