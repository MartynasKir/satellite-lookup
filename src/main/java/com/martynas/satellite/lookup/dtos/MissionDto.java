package com.martynas.satellite.lookup.dtos;

import com.martynas.satellite.lookup.entities.Satellite;
import java.util.List;

public class MissionDto {

    private final String name;
    private final List<Satellite> satellites;

    public MissionDto(String name, List<Satellite> satellites) {
        this.name = name;
        this.satellites = satellites;
    }

    public String getName() {
        return name;
    }

    public List<Satellite> getSatellites() {
        return satellites;
    }
}
