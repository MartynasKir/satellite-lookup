package com.martynas.satellite.lookup.services;

import com.martynas.satellite.lookup.entities.SatelliteIdentificator;
import com.martynas.satellite.lookup.exceptions.RestApiException;
import com.martynas.satellite.lookup.repositories.SatelliteIdentificatorRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import javax.ws.rs.core.Response.Status;


@ApplicationScoped
@Transactional(TxType.REQUIRED)
public class SatelliteIdentificatorService {

    @Inject
    SatelliteIdentificatorRepository satelliteIdentificatorRepository;

    public String findSatelliteDomainId(String satelliteId, String domain) {
        if (satelliteIdentificatorRepository.findBySatelliteIdAndDomain(satelliteId,domain)==null){
            throw new RestApiException(Status.NOT_FOUND);
        }

        return satelliteIdentificatorRepository.findBySatelliteIdAndDomain(satelliteId,domain);
    }

    public List<SatelliteIdentificator> findAllSatellites(){
        return satelliteIdentificatorRepository.findAllRecords();
    }

    public SatelliteIdentificator addSatelliteIdentificatorRecord(SatelliteIdentificator satelliteIdentificator){
        if(satelliteIdentificatorRepository.findBySatelliteId(satelliteIdentificator.getSatelliteId())!=null){
            throw new RestApiException(Status.CONFLICT);
        }
        satelliteIdentificatorRepository.persist(satelliteIdentificator);
        return satelliteIdentificatorRepository.findBySatelliteId(satelliteIdentificator.getSatelliteId());
    }

    public int updateSatelliteIdentificatorRecord(SatelliteIdentificator satelliteIdentificator){
        if (satelliteIdentificatorRepository.findBySatelliteId(satelliteIdentificator.getSatelliteId())==null) {
            throw new RestApiException(Status.NOT_FOUND);
        }
        return satelliteIdentificatorRepository.updateRecord(satelliteIdentificator);
    }

    public void deleteSatelliteIdentificatorRecord(String satelliteId) {
        if (satelliteIdentificatorRepository.findBySatelliteId(satelliteId)==null) {
            throw new RestApiException(Status.NOT_FOUND);
        }
        satelliteIdentificatorRepository.deleteRecordBySatelliteId(satelliteId);
    }
}
