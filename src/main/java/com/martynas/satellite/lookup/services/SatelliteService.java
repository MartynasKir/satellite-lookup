package com.martynas.satellite.lookup.services;

import com.martynas.satellite.lookup.entities.Satellite;
import com.martynas.satellite.lookup.exceptions.RestApiException;
import com.martynas.satellite.lookup.repositories.SatelliteRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.core.Response.Status;

@ApplicationScoped
@Transactional
public class SatelliteService {

    @Inject
    SatelliteRepository satelliteRepository;


    public Satellite addSatellite(@Valid Satellite satellite) {

        if (satelliteRepository.findByInternationalDesignator(satellite.getInternationalDesignator())!=null) {
            throw new RestApiException(Status.CONFLICT);
        }
        satelliteRepository.persist(satellite);

        return satelliteRepository.findByInternationalDesignator(satellite.getInternationalDesignator());
    }


    public Satellite findSatelliteById(String internationalDesignator) {
        return satelliteRepository.findByInternationalDesignator(internationalDesignator);
    }

    public List<Satellite> findSatelliteByMultipleId(List<String> internationalDesignator) {
        return satelliteRepository.findByMultipleInternationalDesignator(internationalDesignator);
    }


    public List<Satellite> findAllSatellites() {
        return satelliteRepository.listAll();
    }


    public int updateSatellite(Satellite satellite) {
        if (satelliteRepository.findByInternationalDesignator(satellite.getInternationalDesignator())==null) {
            throw new RestApiException(Status.NOT_FOUND);
        }
        return satelliteRepository.updateSatellite(satellite);
    }


    public void deleteSatellite(String internationalDesignator) {
        if (satelliteRepository.findByInternationalDesignator(internationalDesignator)==null) {
            throw new RestApiException(Status.NOT_FOUND);
        }
        satelliteRepository.deleteByIternationalDesignator(internationalDesignator);
    }
}
