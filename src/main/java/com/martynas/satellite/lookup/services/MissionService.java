package com.martynas.satellite.lookup.services;

import com.martynas.satellite.lookup.dtos.MissionDto;
import com.martynas.satellite.lookup.entities.Satellite;
import com.martynas.satellite.lookup.repositories.MissionRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MissionService {

    @Inject
    SatelliteService satelliteService;

    @Inject
    MissionRepository missionRepository;

    public MissionDto findSatellitesByName(String missionName){
        List<String> satelliteIds = missionRepository.findSatelliteIdsByMissionName(missionName);
        List<Satellite> satellites = satelliteService.findSatelliteByMultipleId(satelliteIds);
        return new MissionDto(missionName, satellites);
    }
}
