package com.martynas.satellite.lookup.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "satellites_in_domain", schema = "public")
public class SatelliteIdentificator {

    @Id
    @Column(name = "satellite_id")
    private String satelliteId;
    @Column(name = "domain")
    private String domain;
    @Column(name = "satellite_id_in_domain")
    private String satelliteIdInDomain;

    public String getSatelliteId() {
        return satelliteId;
    }

    public String getDomain() {
        return domain;
    }

    public String getSatelliteIdInDomain() {
        return satelliteIdInDomain;
    }
}
