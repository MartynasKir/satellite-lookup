package com.martynas.satellite.lookup.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "missions", schema = "public")
public class Mission {
    @Id
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "satellite_id")
    private String satelliteId;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSatelliteId() {
        return satelliteId;
    }
}
