package com.martynas.satellite.lookup.entities;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "satellite", schema = "public")
public class Satellite extends PanacheEntityBase {

    @Id
    @Column(name = "international_designator")
    private String internationalDesignator;
    @Column(name = "name")
    private String name;
    @Column(name = "year_of_launch")
    private int yearOfLaunch;
    @Column(name = "orbit_type")
    private String orbitType;

    public void setInternationalDesignator(String internationalDesignator) {
        this.internationalDesignator = internationalDesignator;
    }

    public String getInternationalDesignator() {
        return internationalDesignator;
    }

    public String getName() {
        return name;
    }

    public int getYearOfLaunch() {
        return yearOfLaunch;
    }

    public String getOrbitType() {
        return orbitType;
    }
}
