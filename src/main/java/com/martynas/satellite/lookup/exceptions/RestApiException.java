package com.martynas.satellite.lookup.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response.Status;

public class RestApiException extends ClientErrorException {

    public RestApiException(Status status) {
        super(status);
    }
}
