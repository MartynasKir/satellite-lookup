package com.martynas.satellite.lookup.repositories;

import com.martynas.satellite.lookup.entities.Satellite;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SatelliteRepository implements PanacheRepository<Satellite> {

    private final String INTERNATIONAL_DESIGNATOR = "international_designator";

    public Satellite findByInternationalDesignator(String internationalDesignator) {
        return find(INTERNATIONAL_DESIGNATOR, internationalDesignator).firstResult();
    }

    public List<Satellite> findByMultipleInternationalDesignator(List<String> internationalDesignatorList) {

        return list("international_designator in :satellite_id_list",
                Parameters.with("satellite_id_list", internationalDesignatorList));
    }

    public int updateSatellite(Satellite satellite){
        return update(
                "name = :name, year_of_launch = :year_of_launch, orbit_type = :orbit_type "
                        + "where international_designator = :international_designator",
                Parameters
                        .with(INTERNATIONAL_DESIGNATOR,satellite.getInternationalDesignator())
                        .and("name",satellite.getName())
                        .and("year_of_launch", satellite.getYearOfLaunch())
                        .and("orbit_type",satellite.getOrbitType())
        );
    }

    public void deleteByIternationalDesignator(String internationalDesignator){
        delete("international_designator = :international_designator ",
                Parameters.with(INTERNATIONAL_DESIGNATOR,internationalDesignator));
    }
}
