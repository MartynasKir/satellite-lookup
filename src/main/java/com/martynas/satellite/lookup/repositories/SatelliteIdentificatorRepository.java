package com.martynas.satellite.lookup.repositories;

import com.martynas.satellite.lookup.entities.SatelliteIdentificator;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class SatelliteIdentificatorRepository implements PanacheRepository<SatelliteIdentificator> {

    private final String SATELLITE_ID = "satellite_id";

    public SatelliteIdentificator findBySatelliteId(String satelliteId) {
        return find(SATELLITE_ID, satelliteId).firstResult();
    }

    public String findBySatelliteIdAndDomain(String satelliteId, String domain){
        return find(
                "satellite_id = :id and domain = :domain",
                Parameters.with("id",satelliteId)
                        .and("domain", domain))
                .firstResult()
                .getSatelliteIdInDomain();
    }

    public List<SatelliteIdentificator> findAllRecords(){
        return listAll();
    }

    public SatelliteIdentificator addRecord(SatelliteIdentificator satelliteIdentificator){
        persist(satelliteIdentificator);
        return findBySatelliteId(satelliteIdentificator.getSatelliteId());
    }

    public int updateRecord(SatelliteIdentificator satelliteIdentificator){
        return update(
                "domain = :domain, satellite_id_in_domain = :satellite_id_in_domain "
                        + "where satellite_id = :satellite_id",
                Parameters
                        .with(SATELLITE_ID,satelliteIdentificator.getSatelliteId())
                        .and("domain",satelliteIdentificator.getDomain())
                        .and("satellite_id_in_domain", satelliteIdentificator.getSatelliteIdInDomain())
        );
    }

    public void deleteRecordBySatelliteId(String satelliteId){
        delete("satellite_id = :satellite_id ",
                Parameters.with(SATELLITE_ID,satelliteId));
    }
}
