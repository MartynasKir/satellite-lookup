package com.martynas.satellite.lookup.repositories;

import com.martynas.satellite.lookup.entities.Mission;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MissionRepository implements PanacheRepository<Mission> {

    public List<String> findSatelliteIdsByMissionName(String missionName){
        return stream("name = :missionName", Parameters.with("missionName", missionName))
                .map(mission -> mission.getSatelliteId())
                .collect(Collectors.toList());
    }

}
