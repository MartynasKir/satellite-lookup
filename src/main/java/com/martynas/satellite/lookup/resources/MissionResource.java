package com.martynas.satellite.lookup.resources;

import com.martynas.satellite.lookup.dtos.MissionDto;
import com.martynas.satellite.lookup.services.MissionService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/v1/mission")
public class MissionResource {

    @Inject
    MissionService missionService;

    @GET
    @Path("{mission}")
    public MissionDto missionDetails(@PathParam("mission") String missionName){
        return missionService.findSatellitesByName(missionName);
    }
}
