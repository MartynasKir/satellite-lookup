package com.martynas.satellite.lookup.resources;

import com.martynas.satellite.lookup.entities.Satellite;
import com.martynas.satellite.lookup.services.SatelliteService;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/v1/satellites")
public class SatelliteResource {

    @Inject
    SatelliteService satelliteService;

    @GET
    @Path("/")
    public List<Satellite> getAllSatellites() {
        return satelliteService.findAllSatellites();
    }

    @GET
    @Path("/{id}")
    public Satellite getSatelliteById(@PathParam("id") String internationalDesignator) {
        return satelliteService.findSatelliteById(internationalDesignator);
    }

    @POST
    @Path("/")
    public Satellite addSatellite(Satellite satellite) {
        return satelliteService.addSatellite(satellite);
    }

    @PUT
    @Path("/")
    public String updateSatellite(Satellite satellite) {
        return "Successfully updated records: " + satelliteService.updateSatellite(satellite);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteSatellite(@PathParam("id") String internationalDesignator) {
        satelliteService.deleteSatellite(internationalDesignator);
        return Response.status(200).build();
    }
}
