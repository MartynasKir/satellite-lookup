package com.martynas.satellite.lookup.resources;

import com.martynas.satellite.lookup.entities.SatelliteIdentificator;
import com.martynas.satellite.lookup.services.SatelliteIdentificatorService;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/v1/satellites/identificators")
public class SatelliteIdentificatorResource {

    @Inject
    SatelliteIdentificatorService satelliteIdentificatorService;

    @GET
    @Path("/{id}/domain/{domain}")
    public String getSatelliteDomainId(@PathParam("id") String id, @PathParam("domain") String domain) {
        return satelliteIdentificatorService.findSatelliteDomainId(id, domain);
    }

    @GET
    @Path("/")
    public List<SatelliteIdentificator> getAllRecords(){
        return satelliteIdentificatorService.findAllSatellites();
    }

    @POST
    @Path("/")
    public SatelliteIdentificator addSatelliteRecord(SatelliteIdentificator satelliteIdentificator){
        return satelliteIdentificatorService.addSatelliteIdentificatorRecord(satelliteIdentificator);
    }

    @PUT
    @Path("/")
    public String updateSatelliteRecord(SatelliteIdentificator satelliteIdentificator){
        return "Successfully updated records: " + satelliteIdentificatorService.updateSatelliteIdentificatorRecord(satelliteIdentificator);
    }

    @DELETE
    @Path("{id}")
    public Response deleteSatelliteRecord(@PathParam("id") String satelliteId){
        satelliteIdentificatorService.deleteSatelliteIdentificatorRecord(satelliteId);
        return Response.status(200).build();
    }
}
