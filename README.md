# satellite-lookup project

This is a demo project of a `satellite look-up` service build with Quarkus framework.
For database management Flyway db migration tool is used.

### How to run this demo:
1. Download project
2. Run `docker compose up` to initiate postgres database
3. Run `flywayMigrate` Gradle task to create and populate tables
4. Add following database configuration to `/build/resources/main/application.properties`
```
quarkus.hibernate-orm.database.generation=none
quarkus.hibernate-orm.log.sql=true
quarkus.datasource.db-kind=postgresql
quarkus.datasource.username=postgres
quarkus.datasource.password=martynas
quarkus.datasource.jdbc.url=jdbc:postgresql://localhost:5432/satellite-demo
```
5. Run `quarkusDev` Gradle task to start test version
6. Test endpoints. Example: `http://localhost:8080/v1/satellites/1992-001A`

# More about quarkus
This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./gradlew quarkusDev
```

## Packaging and running the application

The application can be packaged using:
```shell script
./gradlew build
```
It produces the `satellite-lookup-1.0-SNAPSHOT-runner.jar` file in the `/build` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `build/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./gradlew build -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar build/satellite-lookup-1.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./gradlew build -Dquarkus.package.type=native
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./gradlew build -Dquarkus.package.type=native -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./build/satellite-lookup-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/gradle-tooling.
