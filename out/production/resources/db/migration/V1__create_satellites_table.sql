create table satellite (
    INTERNATIONAL_DESIGNATOR varchar(100) not null,
    NAME varchar(100) not null,
    YEAR_OF_LAUNCH smallint not null,
    ORBIT_TYPE varchar(100) not null
);
